<?php
/**
 * @file
 * paddle_title_icons.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function paddle_title_icons_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_paddle_title_icon'
  $field_bases['field_paddle_title_icon'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_paddle_title_icon',
    'foreign keys' => array(
      'fid' => array(
        'columns' => array(
          'fid' => 'fid',
        ),
        'table' => 'file_managed',
      ),
    ),
    'indexes' => array(
      'value' => array(
        0 => 'fid',
      ),
    ),
    'locked' => 0,
    'module' => 'paddle_icon',
    'settings' => array(
      'icon_managing_module' => 'paddle_title_icons',
    ),
    'translatable' => 0,
    'type' => 'icon',
  );

  return $field_bases;
}
