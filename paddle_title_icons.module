<?php
/**
 * @file
 * Code for the Title Icons app.
 */

include_once 'paddle_title_icons.features.inc';

define('PADDLE_TITLE_ICONS_CSS_ID', 'paddle-title-icons-css');

/**
 * Maximum image width allowed for the icons. Used by upload validators.
 */
define('PADDLE_TITLE_ICONS_MAX_IMAGE_WIDTH', 64);

/**
 * Maximum image height allowed for the icons. Used by upload validators.
 */
define('PADDLE_TITLE_ICONS_MAX_IMAGE_HEIGHT', 64);

/**
 * Implements hook_apps_app_info().
 */
function paddle_title_icons_apps_app_info() {
  return array(
    'configure form' => 'paddle_title_icons_settings_form',
  );
}

/**
 * Form callback displaying Title Icons settings.
 */
function paddle_title_icons_settings_form($form, &$form_state) {
  $form = array(
    '#prefix' => '<div id="title-icons-form-wrapper">',
    '#suffix' => '</div>',
  );

  if (module_exists('paddle_contextual_toolbar')) {
    $form['#after_build'][] = 'paddle_title_icons_add_contextual_actions_settings_form';
  }

  $form['upload_form'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $allowed_dimensions = PADDLE_TITLE_ICONS_MAX_IMAGE_WIDTH . 'x' . PADDLE_TITLE_ICONS_MAX_IMAGE_HEIGHT;

  $form['upload_form']['icon_upload'] = array(
    '#title' => t('Upload an icon'),
    '#type' => 'managed_file',
    '#default_value' => NULL,
    '#upload_location' => 'public://paddle_title_icons/icons',
    '#upload_validators' => array(
      'file_validate_is_image' => array(),
    ),
  );

  $form['upload_form']['add_uploaded_icon'] = array(
    '#type' => 'submit',
    '#value' => t('Add the icon'),
    '#submit' => array('paddle_title_icons_add_uploaded_icon'),
    '#ajax' => array(
      'callback' => 'paddle_title_icons_rebuild_form',
      'wrapper' => 'title-icons-form-wrapper',
      'effect' => 'fade',
    ),
    '#attributes' => array(
      'class' => array(
        'add-icon',
      ),
    ),
  );

  $icons = paddle_icon_get_icon_files('paddle_title_icons');

  if (count($icons)) {
    $form['icons_list'] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
  }
  foreach ($icons as $icon) {
    $form['icons_list'][$icon->fid] = array(
      '#type' => 'container',
      '#tree' => TRUE,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    );
    $form['icons_list'][$icon->fid]['icon_image'] = array(
      '#markup' => theme('image', array(
        'path' => image_style_url('paddle_title_icons_title', $icon->uri),
        'alt' => $icon->filename,
        'title' => $icon->filename,
        'attributes' => array(
          'class' => 'uploaded-icon-image',
        ),
      )),
    );
    // We need to set the "#name" because otherwise it's just taking the last
    // button as the triggering button.
    $form['icons_list'][$icon->fid]['remove_icon'] = array(
      '#type' => 'submit',
      '#value' => t('Remove the icon'),
      '#submit' => array('paddle_title_icons_remove_uploaded_icon'),
      '#name' => 'remove_icon_' . $icon->fid,
      '#ajax' => array(
        'callback' => 'paddle_title_icons_rebuild_form',
        'wrapper' => 'title-icons-form-wrapper',
        'effect' => 'fade',
      ),
    );
  }

  return $form;
}

/**
 * Submit callback for "Remove the icon" button.
 */
function paddle_title_icons_remove_uploaded_icon($form, &$form_state) {
  // Get the fid from the parent container name where we store it.
  $icon_fid = $form_state['clicked_button']['#parents'][1];
  if ($icon_fid && $file = file_load($icon_fid)) {
    file_usage_delete($file, 'paddle_title_icons', 'icon');
    file_delete($file);
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit callback for "Add the icon" button.
 */
function paddle_title_icons_add_uploaded_icon($form, &$form_state) {
  $icon_fid = $form_state['values']['icon_upload'];
  if ($icon_fid && $file = file_load($icon_fid)) {
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'paddle_title_icons', 'icon', $icon_fid);

    paddle_title_icons_css_file_clear();
  }
  // We want a clean the upload field.
  $form_state['values']['icon_upload'] = NULL;
  $form_state['input']['icon_upload']['fid'] = NULL;
  $form_state['rebuild'] = TRUE;
}

/**
 * AJAX callback which rebuilds the form after icon has been added or removed.
 *
 * @return array
 *   The form.
 */
function paddle_title_icons_rebuild_form($form, &$form_state) {
  return $form;
}

/**
 * After-build function of the Title Icons settings form.
 */
function paddle_title_icons_add_contextual_actions_settings_form($form, $form_state) {
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();
  paddle_contextual_toolbar_add_js('click_delegator');

  unset($form['actions']['preview']);

  $actions = array();
  $actions[] = array(
    'action' => l(t('Cancel'), 'admin/apps/paddle_apps/paddle_title_icons'),
    'class' => array('cancel'),
    'weight' => -10,
  );

  if (!module_exists('paddle_title_icons')) {
    $actions[] = array(
      'action' => l(t('Activate'), 'admin/apps/paddle_apps/paddle_title_icons/enable'),
      'class' => array('activate'),
      'weight' => 10,
    );
  }
  else {
    $actions[] = array(
      'action' => l(t('Deactivate'), 'admin/apps/paddle_apps/paddle_title_icons/disable'),
      'class' => array('deactivate'),
      'weight' => 10,
    );
  }

  paddle_contextual_toolbar_actions($actions);

  return $form;
}

/**
 * Implements hook_modules_enabled().
 */
function paddle_title_icons_modules_enabled($modules) {
  paddle_title_icons_refresh_field_instances();
}

/**
 * Create field instances of the title icon field for all node bundles.
 */
function paddle_title_icons_refresh_field_instances() {
  // If the module containing the required field type gets enabled together
  // with this module, we need to load it explicitly.
  drupal_load('module', 'paddle_icon');

  // Ensure our base field is there.
  features_include_defaults('field_base', TRUE);
  $rebuild = array(
    'paddle_title_icons' => array(
      'field_base',
    ),
  );
  features_rebuild($rebuild);

  foreach (node_type_get_types() as $node_type) {
    // Skip adding field instance if it already exists.
    $field_instance = field_info_instance('node', 'field_paddle_title_icon', $node_type->type);
    if ($field_instance) {
      continue;
    }

    $field_instance = array(
      'bundle' => $node_type->type,
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 5,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_paddle_title_icon',
      'label' => 'Icon',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'paddle_title_icons',
        'settings' => array(),
        'type' => 'icon_select',
        'weight' => 7,
      ),
    );

    field_create_instance($field_instance);
  }

  // Translatables.
  t('Icon');
}

/**
 * Implements hook_paddle_content_manager_additional_fields().
 *
 * @return array
 *   The additional fields added by this module.
 */
function paddle_title_icons_paddle_content_manager_additional_fields() {
  return array('field_paddle_title_icon');
}

/**
 * Implements hook_preprocess_html().
 */
function paddle_title_icons_preprocess_html(&$variables) {
  $node = menu_get_object('node');

  $menu_item = menu_get_item();
  // Unfortunately paddle content manager's iframe preview is not handling
  // things in a pretty way, so need to foresee a workaround here
  // until it is fixed there.
  if ($menu_item['path'] == 'paddle_content_manager/node_iframe/%') {
    $nid = arg(2);

    $node = node_load($nid);
  }

  if (!$node) {
    return;
  }

  $items = field_get_items('node', $node, 'field_paddle_title_icon');

  if (FALSE !== $items) {
    $item = reset($items);

    if ($item && $item['fid']) {
      $variables['classes_array'][] = 'paddle-title-icon-applied';
      $variables['classes_array'][] = paddle_title_icons_icon_class($item['fid']);
    }
  }
}

/**
 * Generate a HTML class suitable for marking elements with an icon.
 *
 * @param int $fid
 *   The icon managed_file id.
 *
 * @return string
 *   The HTML class.
 */
function paddle_title_icons_icon_class($fid) {
  return drupal_html_class('node-title-icon-' . $fid);
}

/**
 * Implements hook_init().
 */
function paddle_title_icons_init() {
  // Only apply to frontend.
  if (path_is_admin(current_path())) {
    return;
  }

  $filename = paddle_title_icons_css_file();

  if ($filename) {
    // Add the css.
    $options = array(
      'type' => 'file',
      'group' => CSS_THEME,
      'every_page' => TRUE,
    );

    drupal_add_css($filename, $options);
  }
}

/**
 * Generates the necessary CSS and stores it in a file with the ctools CSS API.
 *
 * @return NULL|string
 *   The path to the CSS file, or NULL if no CSS needs to be added.
 */
function paddle_title_icons_css_file() {
  ctools_include('css');

  $filename = ctools_css_retrieve(PADDLE_TITLE_ICONS_CSS_ID);

  if (!$filename) {
    $css = array();

    $icons = paddle_icon_get_icon_files('paddle_title_icons');
    foreach ($icons as $icon) {
      $css[] = paddle_title_icons_title_css($icon);
    }

    if (!empty($css)) {
      $css[] = <<<EOB
body.paddle-title-icon-applied #page-title {
  text-indent: 60px;
  padding: 10px 0;
}

#block-paddle-menu-display-footer-menu .level-1 > li.paddle-title-icon-applied > a,
#block-paddle-menu-display-full-vertical .level-1 > li.paddle-title-icon-applied > a {
  padding: 4px 0 4px 35px;
}
EOB;

      $css = implode("\r\n", $css);

      // Cache the CSS and get the filename.
      $filename = ctools_css_store(PADDLE_TITLE_ICONS_CSS_ID, $css, FALSE);
    }
  }

  return $filename;
}

/**
 * Generates CSS to put an icon in front of the page title.
 *
 * @param array $icon_file
 *   The managed icon file.
 *
 * @return string
 *   A CSS string.
 */
function paddle_title_icons_title_css($icon_file) {
  $icon_class = paddle_title_icons_icon_class($icon_file->fid);

  $title_icon_url = image_style_url('paddle_title_icons_title', $icon_file->uri);
  $menu_icon_url = image_style_url('paddle_title_icons_menu', $icon_file->uri);

  $css = <<<EOB
body.{$icon_class} #page-title {
  background: url({$title_icon_url}) no-repeat bottom left;
}

#block-paddle-menu-display-footer-menu .level-1 > li.{$icon_class} > a,
#block-paddle-menu-display-full-vertical .level-1 > li.{$icon_class} > a {
  background: url({$menu_icon_url}) no-repeat top left;
}
EOB;

  return $css;
}

/**
 * Clears the css file, so it will be regenerated on the next request.
 *
 * @see paddle_title_icons_css_file()
 */
function paddle_title_icons_css_file_clear() {
  ctools_css_clear(PADDLE_TITLE_ICONS_CSS_ID);
}

/**
 * Get the title icon from a node.
 *
 * @param stdClass $node
 *   The node object.
 * 
 * @return mixed|null
 *   The title icon field item, or NULL if no icon was set.
 */
function paddle_title_icons_get_node_title_icon($node) {
  $items = field_get_items('node', $node, 'field_paddle_title_icon');

  $icon = NULL;
  if (FALSE !== $items) {
    $icon = reset($items);
  }

  return $icon;
}

/**
 * Implements hook_paddle_menu_display_menu_items_alter().
 */
function paddle_title_icons_paddle_menu_display_menu_items_alter(&$items, $menu_display) {
  if (in_array($menu_display->name, array('footer_menu', 'full_vertical'))) {
    foreach ($items as &$item) {
      if ($item['menu_item']['router_path'] == 'node/%') {
        $node = menu_get_object('node', 1, $item['menu_item']['link_path']);

        $icon = paddle_title_icons_get_node_title_icon($node);

        if ($icon && $icon['fid']) {
          $classes = array('paddle-title-icon-applied', paddle_title_icons_icon_class($icon['fid']));
          $item['li_class'] .= ' ' . implode(' ', $classes);
        }
      }
    }
  }
}
