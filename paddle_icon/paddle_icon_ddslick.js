(function($) {
Drupal.behaviors.paddle_icon_ddslick = {
  attach: function(context, settings) {
    for (var id in settings.paddle_icon_ddslick) {
      var icon_urls = settings.paddle_icon_ddslick[id].icon_urls;

      var select = $('#' + id);

      // Do not do anything if the element is not found (yet).
      if (select.length > 0) {
        select.find('option').once(function (index, option) {
          var fid = $(option).attr('value');

          if (fid && (fid in icon_urls)) {
            $(option).attr('data-imagesrc', icon_urls[fid]);
          }
        });

        // The ddslick plugin replaces the select element,
        // therefore we can not use .once() on the select element itself,
        // because it would still potentially run more than once.
        $('body').once(id + '-ddslick', function() {
          select.ddslick();
        });
      }
    }
  }
}
})(jQuery);

