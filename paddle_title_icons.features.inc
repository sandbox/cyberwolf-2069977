<?php
/**
 * @file
 * paddle_title_icons.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function paddle_title_icons_image_default_styles() {
  $styles = array();

  // Exported image style: paddle_title_icons_menu.
  $styles['paddle_title_icons_menu'] = array(
    'name' => 'paddle_title_icons_menu',
    'label' => 'Paddle Title Icons menu',
    'effects' => array(
      2 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 25,
          'height' => 25,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: paddle_title_icons_title.
  $styles['paddle_title_icons_title'] = array(
    'name' => 'paddle_title_icons_title',
    'label' => 'Paddle Title Icons title',
    'effects' => array(
      1 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 49,
          'height' => 49,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
